import "./App.css";
import { ApolloProvider, ApolloClient, InMemoryCache, HttpLink } from "@apollo/client";
import { Countries } from "./components/countries";


function App() {
  const uri = "https://countries.trevorblades.com/";
  const client = new ApolloClient({
    link: new HttpLink({ uri: uri }),
    cache: new InMemoryCache(),
  });

  return (
    <ApolloProvider client={client}>
      <div className="App"></div>
      <Countries/>
    </ApolloProvider>
  );
}

export default App;

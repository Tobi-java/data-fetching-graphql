import React from "react";
import { gql, useQuery } from "@apollo/client";
import "./countries.styles.css";

const GET_DATA = gql`
  query MyQuery {
    countries {
      name
      capital
      code
      phone
    }
  }
`;

export const Countries = () => {
  const { data, loading, error } = useQuery(GET_DATA);

  if (loading) return "Loading...";
  if (error) return <pre>{error.message}</pre>;

  return (
    <div>
      <h1 className="heading">COUNTRIES</h1>
      <table>
        <thead className="head">
          <tr className="head-row">
            <th className="th-row">Name</th>
            <th className="th-row">Capital</th>
            <th className="th-row">Code</th>
            <th className="th-row">Phone</th>
          </tr>
        </thead>
        <tbody className="body">
          {data.countries.map((data) => (
            <tr className="body-row">
              <td className="row">{data.name}</td>
              <td className="row">{data.capital}</td>
              <td className="row">{data.code}</td>
              <td className="row">{data.phone}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export const columns = [
    {
      Header: "Name",
      accessor: "name",
    },
    {
      Header: "Capital",
      accessor: "capital",
    },
    {
      Header: "Code",
      accessor: "code",
    },
    {
      Header: "Phone",
      accessor: "phone",
    },
  ];
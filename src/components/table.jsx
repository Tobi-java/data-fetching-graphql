import React, { useMemo } from "react";

import { useTable } from "react-table";
import { gql, useQuery } from "@apollo/client";
import { columns } from "./columns";

const GET_DATA = gql`
  query MyQuery {
    countries {
      name
      capital
      code
      phone
    }
  }
`;

export const Table = () => {

    const {data, loading, error} = useQuery(GET_DATA)
 
    
  const column = useMemo(() => columns, []);
  const infos = useMemo(() => data, []);

  const table = useTable({
    columns: column,
    data: infos,
  });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    table;

    if (loading) return "Loading...";
    if (error) return <pre>{error.message}</pre>

  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
